# Final Lo Visto

Práctica final del curso 2020/21

## Datos

* Nombre: Javier Moreno Revenga
* Titulación: Ing. Tecnologías de la Telecomunicación
* Nombre del laboratorio: jmoreno
* URL vídeo parte obligatoria:  https://www.youtube.com/watch?v=z2Mgr6pZ774
* URL vídeo parte opcional: https://www.youtube.com/watch?v=q1ac5iGvFMY
* URL Despliegue (pythonanywhere): http://jmoreno.pythonanywhere.com/

## Cuenta Admin Site

* javier/javier

## Cuentas usuarios

* javier/javier
* pilar/pilar
* pedro/pedro
* cristina/cristina

## Resumen parte obligatoria

He intentado resolver la parte obligatoria y creo que está completa, salvo algún detalle.
Se podrían mejorar los recursos HTML, haciendo que el banner cuelgue de una página html, pero no lo he implementado así.
Podía también haberse utilizado más las herramientas de Bootstrap a nivel de maquetación.

## Lista de partes opcionales

* Inclusión de elemento favicon del sitio.
* Implementación de todos los recursos reconocidos que se solicitaban en las especificaciones de la práctica (Reddit, YT, Wiki y AEMET)
* Implementación de un sistema para visualizar la página de usuario de todos los usuarios existentes en la base de datos, enumerados en la página principal de Lo Visto.
* Implementación de un campo visitas, que permite contabilizar el número de visitas que se han hecho a la aportación en particular.


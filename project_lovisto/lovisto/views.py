from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from .models import Usuario, Voto, Contenido, Comentario
from django.utils import timezone
from django.contrib.auth import authenticate, logout, login
import xmltodict
import urllib.request
import json
import requests
from bs4 import BeautifulSoup

#-- GESTIÓN DE RECURSOS RECONOCIDOS (YOUTUBE, WIKI, ETC)
def recurso_YT(recurso):
    url_json = "https://www.youtube.com/oembed?format=json&url=https://www.youtube.com/watch?v=" + recurso
    url = requests.get(url_json)
    text = url.text
    data = json.loads(text)
    aporte = data['title'] + "<br>" + data['html'] + "<br>" + data['author_name']
    return aporte


def recurso_Wiki(recurso):
    url_xml = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=" + recurso + "&prop=extracts&exintro&explaintext"
    url_json = "https://es.wikipedia.org/w/api.php?action=query&titles=" + recurso + "&prop=pageimages&format=json&pithumbsize=100"
    handle = urllib.request.urlopen(url_xml)
    d = xmltodict.parse(handle)
    aporte = d['api']['query']['pages']['page']['extract']['#text']
    id_wiki = d['api']['query']['pages']['page']['@pageid']
    if len(aporte) > 400:
        aporte = "<p>" + aporte[:400] + " [...]" + "</p>"

    url = requests.get(url_json)
    text = url.text
    data = json.loads(text)

    imagen = data['query']['pages'][id_wiki]['thumbnail']['source']
    html_imagen = "<img src= '" + imagen + "' width='250' height='250'>"
    url_original = "https://es.wikipedia.org/wiki/" + recurso
    aporte = "<br>" + html_imagen + "<br>" + aporte + "<br><p>Copyright Wikipedia ---> <a href= '" + url_original + "'>Artículo original</a></p>"
    return aporte


def recurso_AEMET(recurso):
    url_xml = "https://www.aemet.es/xml/municipios/localidad_" + recurso + ".xml"
    handle = urllib.request.urlopen(url_xml)
    d = xmltodict.parse(handle)
    dia = d['root']['prediccion']['dia']
    variables_dias = []
    for dia in d['root']['prediccion']['dia']:
        variables_dias.append("Estadísticas para el: " + dia['@fecha'])
        variables_dias.append(
            "Temperaturas (min/max): " + dia['temperatura']['minima'] + "/" + dia['temperatura']['maxima'])
        variables_dias.append(
            "Sensacion (min/max): " + dia['sens_termica']['minima'] + "/" + dia['sens_termica']['maxima'])
        variables_dias.append(
            "Humedad relativa (min/max): " + dia['humedad_relativa']['minima'] + "/" + dia['humedad_relativa'][
                'maxima'])
        variables_dias.append("--------------------------------------------------------------------------------")

    StringDias = ""
    for day in variables_dias:
        StringDias = StringDias + "<li>" + day + "</li>"

    aporte = "<p>Datos AEMET para " + d['root']['nombre'] + "(" + d['root']['provincia'] + ") </p><br>" + StringDias
    url_original = "http://www.aemet.es/es/eltiempo/prediccion/municipios/" + d['root']['nombre'] + "-id" + recurso
    aporte = "<br>" + aporte + "<br><p>Copyright AEMET ---> <a href= '" + url_original + "'>Artículo original</a></p>"
    return aporte


def recurso_Reddit(recurso, subredit):
    url_json = "https://www.reddit.com/r/" + subredit + "/comments/" + recurso + "/.json"
    page = urllib.request.urlopen(url_json)
    text = page.read().decode('utf-8')
    data = json.loads(text)
    url = data[0]['data']['children'][0]['data']['url']
    title = data[0]['data']['children'][0]['data']['title']
    subreddit = data[0]['data']['children'][0]['data']['subreddit']
    selftext = data[0]['data']['children'][0]['data']['selftext']
    upvote = str(data[0]['data']['children'][0]['data']['upvote_ratio'])
    if not ("https://i.redd.it/") in url:
        aporte = "<br>" + title + "<br>" + selftext + "<br>" + "<a href= '" + url + "'>Publicado en: " + subreddit + "</a>" + "<br>" + "Aportación: " + upvote + "<br>"
    else:
        aporte = "<br>" + title + "<br>" + "<img src= '" + url + "' width='250' height='250'>" + "<br>" + "<a href= '" + url + "'>Publicado en: " + subreddit + "</a>" + "<br>" + "Aportación: " + upvote + "<br>"

    return aporte


#-- GESTIÓN DE RECURSOS NO RECONOCIDOS
def recurso_NoReconocido(url):
    aporte = ""
    page = urllib.request.urlopen(url)
    if not page:
        aporte = "Información extendida no disponible"
    else:
        soup = BeautifulSoup(page, 'html.parser')
        ogTitle = soup.find('meta', property='og:title')
        if ogTitle:
            ogImage = soup.find('meta', property='og:image')
            if ogImage:
                imagen = ogImage['content']
                aporte = "<br>" + ogTitle['content'] + "<br><img src= '" + imagen + "' width='250' height='150'>"

        elif not ogTitle:
            hTitle = soup.title.string
            if hTitle:
                aporte = hTitle
            else:
                aporte = "Información extendida no disponible"

    return aporte


#-- METODO PRINCIPAL
@csrf_exempt
def index(request):
    # -- GESTIONAMOS UN METODO POST
    if request.method == "POST":
        action = request.POST['action']
        if action == "Autenticar":
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
            else:
                context = {
                    'user': user
                }
                return HttpResponse(render(request, 'login_failure.html', context))
        elif action == "Desloguearse":
            logout_view(request)

        # -- CAPTURA DEL ENLACE DE LA APORTACIÓN Y PROCEDEMOS SEGÚN TIPO (RECONOCIDO O NO)
        elif action == "Realizar aportación":
            titulo = request.POST['titulo']
            descripcion = request.POST['descripcion']
            enlace = request.POST['enlace']
            autor = request.user
            if ("www.aemet.es") in enlace:
                recurso = enlace.split("id")[1]
                aporte = recurso_AEMET(recurso)

            elif ("es.wikipedia.org") in enlace:
                recurso = enlace.split("/")[4]
                aporte = recurso_Wiki(recurso)

            elif ("www.youtube.com") in enlace:
                recurso = enlace.split("=")[1]
                aporte = recurso_YT(recurso)

            elif ("reddit.com") in enlace:
                recurso = enlace.split("/")[6]
                subredit = enlace.split("/")[4]
                aporte = recurso_Reddit(recurso, subredit)
            else:
                url = enlace
                aporte = recurso_NoReconocido(url)

            fecha = timezone.now()
            aportacion = Contenido(autor=autor, titulo=titulo, descripcion=descripcion, aporte=aporte, fecha=fecha, url=enlace)
            aportacion.save()

        #-- ACTIVAR MODO OSCURO/CLARO
        elif action == "Modo oscuro":
            usuario_modo = Usuario.objects.get(usuario=request.user)
            usuario_modo.modo_oscuro = True
        elif action == "Modo claro":
            usuario_modo = Usuario.objects.get(usuario=request.user)
            usuario_modo.modo_oscuro = False

    elif request.method == "GET":
        content_list = Contenido.objects.all()
        content_list = content_list[::-1]
        if len(content_list) > 10:
            content_list = content_list[:10]

        # -- PRESENTAR APORTACIONES EN FORMATO XML/JSON
        if request.GET.get('format') == 'xml':
            context = {
                'content_list': content_list,
            }
            return render(request, 'aportaciones.xml', context, content_type='text/xml')
        elif request.GET.get('format') == 'json':
            context = {
                'content_list': content_list,
            }
            return render(request, 'aportaciones.json', context, content_type='text/json')

    users_list = Usuario.objects.all()
    users_list = list(users_list)

    #-- ORDENAR APORTACIONES (MÁS RECIENTES)
    content_list = Contenido.objects.all()
    content_list = content_list[::-1]

    #-- CAPTURAR TRES ULTIMOS APORTES PARA PIE DE PÁGINA
    if len(content_list) > 3:
        three_last = content_list[:3]
    else:
        three_last = content_list

    # -- 10 ÚLTIMOS APORTES
    if len(content_list) > 10:
        content_list = content_list[:10]

    # -- SOLO PARA USUARIOS AUTENTICADOS, GESTIONAMOS LA APARICION DE BOTONES LIKE Y DISLIKE
    # -- PARA ELLO, ACTUAMOS SOBRE DOS VARIABLES BOOLEANAS DEL CONTENIDO
    # -- (EN EL HTML, DIREMOS SI CONTENIDO TIENE A TRUE DICHA VARIABLE BOOLEANA IS_LIKE O IS_DISLIKE
    # -- PARA MOSTRAR UN BOTON, OTRO O  LOS DOS)
    if request.user.is_authenticated:
        try:
            user_ct = Contenido.objects.filter(autor=request.user)
            user_content = user_ct.all()
            for content in content_list:
                votes_content = Voto.objects.filter(contenido=content)
                for vote in votes_content:
                    if vote.usuario == str(request.user):
                        if vote.voto_like == True:
                            content.is_like = True
                            content.is_dislike = False
                        elif vote.voto_dislike == True:
                            content.is_like = False
                            content.is_dislike = True
                        else:
                            content.is_like = False
                            content.is_dislike = False

            # -- GUARDAMOS EN LOS CAMPOS DE CONTENIDO EL NUMERO DE LIKES Y DISLIKES
            for content in content_list:
                votes_content = Voto.objects.filter(contenido=content)
                content.nlikes = 0
                content.ndislikes= 0
                for vote in votes_content:
                    if vote.voto_like == True:
                        content.nlikes = content.nlikes + 1
                    elif vote.voto_dislike == True:
                        content.ndislikes = content.ndislikes + 1

            # -- 5 ULTIMAS APORTACIONES DEL USUARIO, ORDENADAS POR MÁS RECIENTES
            user_content = user_content[::-1]
            if len(user_content) > 5:
                user_content = user_content[:5]

            usuario_modo = Usuario.objects.get(usuario=request.user)
            votes_content = Voto.objects.all()
        except Usuario.DoesNotExist:
            usuario_modo = Usuario(usuario=request.user, modo_oscuro=False)
            usuario_modo.save()
        except Voto.DoesNotExist:
            votes_content = None
    else:
        user_content = None
        votes_content = None
        usuario_modo = None

    context = {
        'content_list': content_list,
        'user_list': user_content,
        'user': request.user,
        'users': users_list,
        'votes_content': votes_content,
        'three_last': three_last,
        'usuario_modo': usuario_modo,
    }
    return HttpResponse(render(request, 'inicial2.html', context))


@csrf_exempt
def all_users(request, llave):

    # -- METODO DE OPTATIVA, EN LA VARIABLE LLAVE SE ALMACENA EL NOMBRE DE UN USUARIO
    # -- Y SE PASA A UN HTML TODOS LOS DETALLES DE ESE USUARIO (VOTOS, APORTACIONES, ETC)

    queryset = Contenido.objects.filter(autor=llave)
    user_content = queryset.all()
    user_content = user_content[::-1]
    if len(user_content) > 10:
        user_content = user_content[:10]

    querycoment = Comentario.objects.filter(autor_comentario=llave)
    coment_content = querycoment.all()
    coment_content = coment_content[::-1]
    if len(coment_content) > 5:
        coment_content = coment_content[:5]

    queryset2 = Voto.objects.filter(usuario=llave)
    votes_list = queryset2.all()
    votes_list = votes_list[::-1]
    if len(votes_list) > 5:
        votes_list = votes_list[:5]

    content_total = Contenido.objects.all()
    user_mode = Usuario.objects.get(usuario=request.user)
    content_total = content_total[::-1]

    if len(content_total) > 3:
        three_last = content_total[:3]
    else:
        three_last = content_total
    context = {
        'content_list': user_content,
        'coment_list': coment_content,
        'user': request.user,
        'autor': llave,
        'votes_list': votes_list,
        'three_last': three_last,
        'usuario_mode': user_mode,
    }
    return HttpResponse(render(request, 'usuario.html', context))


@csrf_exempt
def aportaciones(request, llave):

    # -- METODO PARA LA GESTION DE LA PAGINA DE UNA APORTACION
    # -- PERMITE AUTENTICARSE, ENVIAR COMENTARIOS O VOTAR LA APORTACION
    # -- INCLUYE UN METODO OPTATIVO PARA CONTABILIZAR LAS VISITAS A UNA APORTACION

    aportacion = Contenido.objects.get(id=llave)
    aportacion.visitas = aportacion.visitas + 1
    aportacion.save()
    if request.method == 'POST':
        action = request.POST['action']
        if action == "Autenticar":
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
            else:
                context = {
                    'user': user
                }
                return HttpResponse(render(request, 'login_failure.html', context))
        elif action == "Enviar comentario":
            cuerpo = request.POST['cuerpo']
            if cuerpo != "":
                fecha = timezone.now()
                cmt = Comentario(contenido=aportacion, cuerpo=cuerpo, fecha=fecha, autor_comentario=request.user)
                cmt.save()
        elif action == "Like":
            # -- NO SE CONTABILIZAN LAS VISITAS POR REDIRECCION DEBIDO A LIKE / DISLIKE
            try:
                vote = Voto.objects.get(usuario=request.user, contenido=aportacion)
                vote.voto_like = True
                vote.voto_dislike = False
                vote.save()
                aportacion.visitas = aportacion.visitas - 1
                aportacion.save()
            except Voto.DoesNotExist:
                vote = Voto(usuario=request.user, contenido=aportacion, voto_like=True)
                vote.save()

        elif action == "Dislike":
            try:
                vote = Voto.objects.get(usuario=request.user, contenido=aportacion)
                vote.voto_like = False
                vote.voto_dislike = True
                vote.save()
                aportacion.visitas = aportacion.visitas - 1
                aportacion.save()
            except Voto.DoesNotExist:
                vote = Voto(usuario=request.user, contenido=aportacion, voto_dislike=True)
                vote.save()

    comentarios = Comentario.objects.filter(contenido=aportacion)
    coment_content = comentarios.all()
    content_list = Contenido.objects.all()
    content_list = content_list[::-1]
    if len(content_list) > 10:
        content_list = content_list[:10]

    if len(content_list) > 3:
        three_last = content_list[:3]
    else:
        three_last = content_list

    if request.user.is_authenticated:
        try:
            # -- COMO EN EL INDEX, ESTAS DOS VARIBALES BOOLEANAS NOS PERMITEN
            # -- CAMBIAR EN LA PAGINA DE APORTACION DE LIKE A DISLIKE Y VICEVERSA

            vote = Voto.objects.get(usuario=request.user, contenido=aportacion)
            if vote.voto_like == True:
                aportacion.is_like = True
                aportacion.is_dislike = False
            elif vote.voto_dislike == True:
                aportacion.is_like = False
                aportacion.is_dislike = True
            else:
                aportacion.is_like = False
                aportacion.is_dislike = False
        except Voto.DoesNotExist:
            vote = Voto(usuario=request.user, contenido=aportacion)
    else:
        vote = None

    general_vote = Voto.objects.filter(contenido=aportacion)
    aportacion.nlikes = 0
    aportacion.ndislikes = 0
    for votes in general_vote:
        if votes.voto_like == True:
            aportacion.nlikes = aportacion.nlikes + 1
        elif votes.voto_dislike == True:
            aportacion.ndislikes = aportacion.ndislikes + 1
    try:
        user_modo = Usuario.objects.get(usuario=request.user)
    except Usuario.DoesNotExist:
        user_modo = None
    context = {
        'aportacion': aportacion,
        'comentarios': coment_content,
        'user': request.user,
        'user_modo': user_modo,
        'voto': vote,
        'three_last': three_last,
    }
    return HttpResponse(render(request, 'aportacion.html', context))

# -- METODOS PARA LA GESTION DE LIKES Y DISLIKES A NIVEL DEL MODELO VOTO
@csrf_exempt
def gestion_likes(request, clave):
    contenido = Contenido.objects.get(id=clave)
    try:
        voto = Voto.objects.get(usuario=request.user, contenido=contenido)
        voto.voto_like = True
        voto.voto_dislike = False
        voto.save()
    except Voto.DoesNotExist:
        voto = Voto(usuario=request.user, contenido=contenido, voto_like=True)
        voto.save()
    return redirect('/')

@csrf_exempt
def gestion_dislikes(request, clave):
    contenido = Contenido.objects.get(id=clave)
    try:
        voto = Voto.objects.get(usuario=request.user, contenido=contenido)
        voto.voto_like = False
        voto.voto_dislike = True
        voto.save()
    except Voto.DoesNotExist:
        voto = Voto(usuario=request.user, contenido=contenido, voto_dislike=True)
        voto.save()
    return redirect('/')


# -- METODO PARA VISUALIZAR /USER (EL USUARIO QUE ESTA ACTUALMENTE AUTENTICADO
# -- CON SUS COMENTARIOS, VOTOS Y APORTACIONES
@csrf_exempt
def pagina_usuario(request):
    queryuser = Contenido.objects.filter(autor=request.user)
    user_content = queryuser.all()
    querycoment = Comentario.objects.filter(autor_comentario=request.user)
    user_coment = querycoment.all()
    queryvote = Voto.objects.filter(usuario=request.user)
    user_vote = queryvote.all()
    content_total = Contenido.objects.all()
    content_total = content_total[::-1]
    user_modo = Usuario.objects.get(usuario=request.user)

    user_content = user_content[::-1]

    if len(content_total) > 3:
        three_last = content_total[:3]
    else:
        three_last = content_total

    context = {
        'content_list': user_content,
        'coment_list': user_coment,
        'votes_list': user_vote,
        'three_last': three_last,
        'usuario_modo': user_modo,
    }
    return HttpResponse(render(request, 'user.html', context))


# -- METODO PARA VISUALIZAR TODAS LAS APORTACIONES A TRAVÉS DE UNA
# -- INVOCACION A /APORTACIONES
@csrf_exempt
def total_aportaciones(request):
    if request.method == "POST":
        action = request.POST['action']
        if action == "Autenticar":
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
            else:
                context = {
                    'user': user
                }
                return HttpResponse(render(request, 'login_failure.html', context))
    try:
        content_total = Contenido.objects.all()
        content_total = content_total[::-1]
        content_list = content_total
        user_modo = Usuario.objects.get(usuario=request.user)

    except Usuario.DoesNotExist:
        user_modo = None

    if len(content_list) > 3:
        three_last = content_list[:3]
    else:
        three_last = content_total
    context = {
        'content_list': content_total,
        'three_last': three_last,
        'usuario_modo': user_modo,
    }
    return HttpResponse(render(request, 'totalaport.html', context))

# -- METODOS PARA CAMBIAR A MODO CLARO/MODO OSCURO
@csrf_exempt
def modo_oscuro(request):
    prueba = Usuario.objects.get(usuario=request.user)
    prueba.modo_oscuro = True
    prueba.save()
    return redirect('/')


@csrf_exempt
def modo_claro(request):
    prueba = Usuario.objects.get(usuario=request.user)
    prueba.modo_oscuro = False
    prueba.save()
    return redirect('/')


# -- METODO PARA VISUALIZAR LA PÁGINA DE INFORMACION
@csrf_exempt
def informacion(request):
    try:
        user_modo = Usuario.objects.get(usuario=request.user)
    except Usuario.DoesNotExist:
        user_modo = None
    context = {
        'user': request.user,
        'usuario': user_modo
    }
    return HttpResponse(render(request, 'informacion.html', context))


# -- INVOCACION A LOGOUT
@csrf_exempt
def logout_view(request):
    logout(request)
    return redirect('/')

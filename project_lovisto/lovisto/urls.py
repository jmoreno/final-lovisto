from django.urls import path

from . import views

urlpatterns = [
    path('logout/', views.logout_view),
    path('modo_oscuro/', views.modo_oscuro),
    path('modo_claro/', views.modo_claro),
    path('aportacion/<str:llave>', views.aportaciones),
    path('aportaciones/', views.total_aportaciones),
    path('like/<str:clave>', views.gestion_likes),
    path('dislike/<str:clave>', views.gestion_dislikes),
    path('user/', views.pagina_usuario),
    path('informacion/', views.informacion),
    path('', views.index),
    path('<str:llave>', views.all_users),
]

from django.db import models
from datetime import datetime
from django.contrib.auth.models import User

# Create your models here.

class Usuario(models.Model):
    usuario = models.TextField(max_length=64, default='Anonymous')
    modo_oscuro = models.BooleanField(default=False)


class Contenido(models.Model):
    autor = models.TextField(max_length=64, default='Anonymous')
    titulo = models.CharField(max_length=64)
    descripcion = models.TextField(max_length=256, default='Anonymous')
    aporte = models.TextField(blank=False)
    fecha = models.DateTimeField(default=datetime.now, blank=True)
    url = models.CharField(max_length=512, default='Anonymous')
    is_like = models.BooleanField(default=False)
    is_dislike = models.BooleanField(default=False)
    nlikes = models.IntegerField(default=0)
    ndislikes = models.IntegerField(default=0)
    visitas = models.IntegerField(default=0)

    def __str__(self):
        return str(self.id) + " = " + self.titulo


class Comentario(models.Model):
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE)
    cuerpo = models.TextField(blank=False)
    fecha = models.DateTimeField(default=datetime.now, blank=True)
    autor_comentario = models.TextField(max_length=64, default='Anonymous')

class Voto(models.Model):
    usuario = models.CharField(max_length=64, default='Anonymous')
    contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE, default='')
    voto_like = models.BooleanField(default=False)
    voto_dislike = models.BooleanField(default=False)
from django.test import TestCase
from django.test import Client
from .models import Usuario, Voto, Contenido, Comentario
from django.contrib.auth.models import User
from django.utils import timezone

# Create your tests here.

class Test (TestCase):

    def test_login(self):
        cliente = Client()
        usuario = User.objects.create_user("admin", '', "admin").save()
        content = cliente.login(username='admin', password='admin')
        self.assertIn(str(content), 'True')


    def test_logout(self):
        cliente = Client()
        response = cliente.get('/logout/')
        self.assertEqual(response.status_code, 302)


    def test_login_failure(self):
        cliente = Client()
        usuario = User.objects.create_user("admin", '', "admin").save()
        content = cliente.login(username='admin', password='ad')
        self.assertIn(str(content), 'False')


    def test_Principal_GET(self):
        fecha = timezone.now()
        aporte = Contenido(autor="admin", titulo="Testing", descripcion="Testing...", aporte="nuevo aporte", url="http://www.elmundo.com", fecha=fecha, is_like=False, is_dislike=False, nlikes=0, ndislikes=0, visitas=0).save()
        cliente = Client()
        response = cliente.get('')
        self.assertEqual(response.status_code, 200)
        url_prueba = '<a href="' + 'http://www.elmundo.com' + '">http://www.elmundo.com</a>'
        content = response.content.decode('utf-8')
        self.assertIn("<b>Titulo:</b> Testing<br>", content)
        self.assertIn(url_prueba, content)


    def test_Principal_POST(self):
        cliente = Client()
        response = cliente.post('', {'action': "Realizar aportación", 'titulo': "Test", 'descripcion': "Testing API", 'enlace': "http://www.elmundo.com"})
        self.assertEqual(response.status_code, 200)
        url_prueba = '<a href="' + 'http://www.elmundo.com' + '">http://www.elmundo.com</a>'
        content = response.content.decode('utf-8')
        self.assertIn("<b>Titulo:</b> Test<br>", content)
        self.assertIn(url_prueba, content)


    def test_Comentario(self):
        cliente = Client()
        fecha = timezone.now()
        aporte = Contenido(autor="admin", titulo="Testing", descripcion="Testing...", aporte="nuevo aporte", url="http://www.elmundo.com", fecha=fecha, is_like=False, is_dislike=False, nlikes=0, ndislikes=0, visitas=0).save()
        response = cliente.post('/aportacion/1', {'action': "Enviar comentario", 'cuerpo': "Este es el cuerpo"})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        cuerpo_comentario = '<p>' + '"Este es el cuerpo"' + '</p>'
        self.assertIn(cuerpo_comentario, content)


    def test_Aportaciones_GET(self):
        cliente = Client()
        fecha = timezone.now()
        aporte1 = Contenido(autor="admin", titulo="Testing", descripcion="Testing...", aporte="nuevo aporte",
                           url="http://www.elmundo.com", fecha=fecha, is_like=False, is_dislike=False, nlikes=0,
                           ndislikes=0, visitas=0).save()
        aporte2 = Contenido(autor="admin", titulo="Testing2", descripcion="Testing...", aporte="nuevo aporte",
                            url="http://www.marca.com", fecha=fecha, is_like=False, is_dislike=False, nlikes=0,
                            ndislikes=0, visitas=0).save()
        response = cliente.get('/aportaciones/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        url_prueba = '<h6><a href="' + '/aportacion/1' + '">Testing</a></h6>'
        self.assertIn(url_prueba, content)


    def test_Informacion(self):
        cliente = Client()
        response = cliente.get('/informacion/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn("<h1>¿Qué es LoVisto?</h1>", content)


    def votar_Like(self):
        cliente = Client()
        fecha = timezone.now()
        aporte = Contenido(autor="admin", titulo="Testing", descripcion="Testing...", aporte="nuevo aporte",
                            url="http://www.elmundo.com", fecha=fecha, is_like=True, is_dislike=False, nlikes=0,
                            ndislikes=0, visitas=0).save()
        voto = Voto(usuario="admin", contenido=aporte, voto_like=True, voto_dislike=False)
        response = cliente.get('')
        content = response.content.decode('utf-8')
        self.assertIn("<p>La publicación tiene 1 likes y 0 dislikes.</p>", content)


    def votar_Dislike(self):
        cliente = Client()
        fecha = timezone.now()
        aporte = Contenido(autor="admin", titulo="Testing", descripcion="Testing...", aporte="nuevo aporte",
                            url="http://www.elmundo.com", fecha=fecha, is_like=False, is_dislike=True, nlikes=0,
                            ndislikes=0, visitas=0).save()

        voto = Voto(usuario="admin", contenido=aporte, voto_like=False, voto_dislike=True)
        response = cliente.get('')
        content = response.content.decode('utf-8')
        self.assertIn("<p>La publicación tiene 0 likes y 1 dislikes.</p>", content)


    def test_XML(self):
        cliente = Client()
        response = cliente.get('/?format=xml')
        content = response.content.decode('utf-8')
        self.assertIn('<aportaciones>', content)


    def test_JSON(self):
        cliente = Client()
        fecha = timezone.now()
        aporte1 = Contenido(autor="admin", titulo="Testing", descripcion="Testing...", aporte="nuevo aporte",
                            url="http://www.elmundo.com", fecha=fecha, is_like=False, is_dislike=False, nlikes=0,
                            ndislikes=0, visitas=0).save()
        response = cliente.get('/?format=json')
        content = response.content.decode('utf-8')
        self.assertIn("http://www.elmundo.com", content)


from django.contrib import admin
from .models import Usuario, Voto, Contenido, Comentario
# Register your models here.

admin.site.register(Usuario)
admin.site.register(Contenido)
admin.site.register(Comentario)
admin.site.register(Voto)
